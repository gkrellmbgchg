/*
 * GKrellMBgChg: a GKrellM plugin to change the desktop wallpaper
 * Copyright (C) 2002-2010 Stefan Bender
 * Author: Stefan Bender <stefan@bender-suhl.de>
 *
 * This program is free software which I release under the GNU General Public
 * License. You may redistribute and/or modify this program under the terms of
 * that license as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307, USA.
 *
 * For more information, see the README and LICENSE files.
 */
#include <stdio.h>
#include <stdlib.h>
#if !defined(WIN32)
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#endif

#include "gkrellmbgchg.h"

/* Uncomment this or add -DGKBG_DEBUG to compiler's option to see a
   lot of debug output. */
/* #define GKBG_DEBUG */
#ifdef GKBG_DEBUG
#define GKBG_debug(fmt, ...) fprintf(stderr, "debug: " fmt "\n", ##__VA_ARGS__)
#else
#define GKBG_debug(fmt, ...)
#endif

static GtkWidget *gkrellm_vbox;

static GkrellmKrell *krell_time;
static GkrellmPanel *panel;
static GkrellmMonitor *monitor;
static GkrellmDecal *decal_wu;
static GkrellmTicks *pGK;
static gint style_id;

/* all the stuff we need goes into this */
struct bg_ctx {
	GList *idb;
	GList *idb_orig;
	GRand *bgchg_rand;
	GtkTooltips *tip;
	gint32 cur_img;
	gint seconds_left;
	gint locked;
};
/* so we have only this global (still) */
static struct bg_ctx *pbg_ctx;

/* config widgets */
static GtkWidget *entry_format_str;
static GtkWidget *entry_idb;
static GtkWidget *wait_seconds_spin_button;
static GtkWidget *scroll_adj_spin_button;
static GtkWidget *entry_command;
static GtkWidget *parse_cmd_entry;
#if !defined(WIN32)
static GtkWidget *auto_update_entry;
#endif
static GtkWidget *ignore_entry;
static GtkWidget *randomise_entry;
static GtkWidget *reset_entry;
static GtkWidget *reset_entry2;
static GtkWidget *change_on_load;
static GtkWidget *change_on_apply;
static GtkWidget *remember_locked_state;
static GtkWidget *remember_image_number;
static GtkWidget *simple_scroll_adj;
static GtkWidget *center_text;
static GtkWidget *display_text;
static GtkWidget *display_krell;

struct bg_monitor {
	gint wait_seconds;			/* sec. between updates */
	gint randomise;				/* randomise images? */
	gint reset;				/* reset the counter if lock is released */
	gint reset_config;				/* reset the counter on "apply" */
	gchar format_string[128];	/* output format string */
	gchar command[256];			/* command to change bg */
	gboolean parse_cmd_output;	/* parse command's output */
	gchar idb[256];				/* full path to image database */
	gint change_on_load;		/* change image on load */
	gint change_on_apply;		/* change image on config changes */
	gint remember_locked_state;
	gint locked_last_run;		/* was it locked the last time? */
	gint remember_image_number;
	gint32 image_nr_last_run;	/* the number when we last shut down */
	gint simple_scroll_adj;		/* mouse wheel adjusts timer w/o "shift" */
	gint scroll_adj_time;		/* time that the mouse wheel adjusts */
	gboolean center_text;		/* Center time text */
	gboolean display_text;		/* Display text countdown */
	gboolean display_krell;		/* Display krell - slider */
	gboolean ignore;			/* discard files we can not stat */
#if !defined(WIN32)
	gboolean auto_update;			/* auto update image list if changed? */
	time_t idb_mtime;			/* IDB file mtime */
#endif
};

static struct bg_monitor bgmon = {
	.wait_seconds = 600,
	.randomise = 1,
	.reset = 0,
	.reset_config = 0,
	.format_string = "$t",
#if defined(WIN32)
	.command = "",
#else
	.command = "Esetroot -f",
#endif
	.parse_cmd_output = 0,
	.idb = "~/images.idb",
	.change_on_load = 0,
	.change_on_apply = 0,
	.remember_locked_state = 0,
	.locked_last_run = 0,
	.remember_image_number = 0,
	.image_nr_last_run = -1,
	.simple_scroll_adj = 0,
	.scroll_adj_time = 60,
	.center_text = 1,
	.display_text = 1,
	.display_krell = 1,
	.ignore = 0
#if !defined(WIN32)
	,
	.auto_update = 0,
	.idb_mtime = 0
#endif
};

struct idb_entry {
	gchar *filename;
};

/* returns the pointer to the successfully opened file and
 * NULL if something weird happened or the file was not modified
 * and `force' was not set (to 1). */
FILE *open_imagelist( gchar *filename, int force )
{
	FILE *file;
	gchar *tmp;

	if( filename ) {
		if( !strncmp( filename, "~/", MIN(2, strlen(filename)) ) )
			tmp = g_strdup_printf( "%s/%s", g_get_home_dir(), filename+2 );
		else
			tmp = g_strdup_printf( "%s", filename );
	} else return NULL;
	GKBG_debug("bgmon.idb = %s; tmp = %s", bgmon.idb, tmp);

#if !defined(WIN32)
	/* don't load if no force and not modified */
	struct stat buf;
	if( stat( tmp, &buf ) == -1 ) {
		/* something went wrong, we just don't care what for now */
		GKBG_debug("stat: error on `%s'", tmp);
		return NULL;
	}
	if( !force && bgmon.idb_mtime == buf.st_mtime) {
		GKBG_debug("%s was not modified", tmp);
		return NULL;
	}
#endif

	GKBG_debug("opening database `%s'", tmp);
	if( (file = fopen( tmp, "r" )) == NULL) {
		/* fwiw, print an error message to stderr */
		fprintf( stderr, _("Could not open image database. (%s)\n"), _(tmp) );
		if( tmp!=bgmon.idb ) g_free( tmp );
		return NULL;
	}
	if( tmp!=bgmon.idb ) g_free( tmp );

#if !defined(WIN32)
	bgmon.idb_mtime = buf.st_mtime;
#endif

	return file;
}

/* Randomise image list in a non-repeating manner. */
static void randomise_image_list()
{
	GList *gl = NULL;
	GList *entry = NULL, *tentry = NULL;
	guint gll = g_list_length( pbg_ctx->idb );
	int i, tmp;
	gint32 randval;
	gint gli[gll];

	/* Save the original list */
	pbg_ctx->idb_orig = g_list_copy(pbg_ctx->idb);

	/* Randomise the index list */
	for( i = 0; i < gll; i++ ) gli[i] = i;
	for( i = 0; i < gll; i++ ) {
		randval = g_rand_int_range( pbg_ctx->bgchg_rand, 0, gll );
		tmp = gli[i];
		gli[i] = gli[randval];
		gli[randval] = tmp;
	};

	/* save old image entry */
	if( pbg_ctx->cur_img > -1 && pbg_ctx->cur_img < gll )
		entry = g_list_nth( pbg_ctx->idb, pbg_ctx->cur_img );

	/* Create new list from randomised index list */
	for( i = 0; i < gll; i++ )
		gl = g_list_append( gl, ((struct idb_entry *)g_list_nth( pbg_ctx->idb, gli[i] )->data));

	/* exchange entries if remembering is set and the image is in the list */
	if( bgmon.remember_image_number && entry ) {
		i = g_list_index( gl, entry->data);
		tentry = g_list_nth( gl, i );
		gl = g_list_remove_link( gl, tentry );
		gl = g_list_prepend( gl, tentry->data );
	}

	/* Switch to the randomised list */
	g_list_free(pbg_ctx->idb); pbg_ctx->idb = gl;

	/* begin with zero */
	pbg_ctx->cur_img = 0;

	/* Uncomment code below to show indices and filenames */
	/*
	   for( i = 0; i < gll; i++ ) printf(" %d ", gli[i]);
	   fprintf(stderr, "\n");

	   gchar *fname = NULL;
	   for( i = 0; i < gll; i++ ) {
	   fname = g_strdup( ((struct idb_entry *)g_list_nth( pbg_ctx->idb, i )->data)->filename );
	   fprintf(stderr, "%d  <%s>\n", i, fname );
	   }
	   g_free( fname );
	*/
}

/* returns 0 if list was updated (= normal operation), 1 otherwise */
/* force - whether to load list even if not modified */
static int update_image_list(int force)
{
	gchar *tmp = NULL;
#if !defined(MAXPATHLEN)
	gchar *tmp2 = NULL;
#endif
	gchar c;
	gint num=1;
	FILE *idb_file;
	struct idb_entry *idb_e;

	if((idb_file = open_imagelist( bgmon.idb, force )) == NULL) return 1;

	if(pbg_ctx->idb != NULL) { g_list_free(pbg_ctx->idb); pbg_ctx->idb = NULL; }

	tmp = g_malloc( num*BUFFSIZE );
	while(!feof(idb_file)) {

		/* skip leading blanks and tabs */
		while( ((c = fgetc( idb_file )) == 0x20 || c == '\t') && !feof( idb_file ));

		if( c == '#' ) { /* skip entry */
			while( ((char)fgetc( idb_file ) != '\n') && !feof( idb_file ) );
			continue;
		}

		if( c == '\n' ) continue;

		*tmp=c; /* copy entry */
		/* abort on error and eof when no bytes were read */
		if( !fgets( tmp+1, (num*BUFFSIZE)-1, idb_file ) ) continue;
#if !defined(MAXPATHLEN)
		while( tmp[strlen(tmp)-1]!='\n' && !feof( idb_file ) ) {
			if( (tmp2 = g_realloc( tmp, ++num*BUFFSIZE )) == NULL ) break;
			tmp = tmp2;
			if( !fgets( tmp+strlen(tmp), BUFFSIZE, idb_file ) ) break;
		}
#endif
		if( tmp[strlen(tmp)-1]=='\n' )
			tmp[strlen(tmp)-1] = 0x00; /* strip trailing newline */
#if defined(MAXPATHLEN)
		else if( !feof( idb_file ) )
			/* skip to eol or eof */
			while( ((char)fgetc( idb_file ) != '\n') && !feof( idb_file ) );
#endif

		/* ignore the file if doesn't exist */
		if( bgmon.ignore && !g_file_test( tmp, G_FILE_TEST_EXISTS ) ) {
			GKBG_debug( "ignoring `%s'", tmp );
		} else {
			idb_e = (struct idb_entry *)calloc( 1, sizeof( struct idb_entry ));
			idb_e->filename = g_strdup( tmp );
			pbg_ctx->idb = g_list_append( pbg_ctx->idb, idb_e );
		}

	}

	g_free( tmp );
	fclose(idb_file);

	if( bgmon.randomise )
		randomise_image_list();
	else
		pbg_ctx->cur_img = bgmon.image_nr_last_run;

	return 0;
}




static void update_image(gint32 inr)
{
	gchar *fname = NULL, *tiptext = NULL, *ch, *tmp = NULL, *tag, *value;
	gint count = 0;
	double val;
	guint gll = g_list_length( pbg_ctx->idb );

	GKBG_debug("update_image(%i) [%i]", inr, gll);


#if !defined(WIN32)
	/*
	 * Auto update
	 */
	if( bgmon.auto_update && !update_image_list(0)) {
		GKBG_debug("list modified.");
		inr = -1;
	}
#endif


	/*
	 * Choose file
	 */
	/* nothing to change, if there are not at least 2 images,
	   however, if list was just updated image in it may be different. */
	if( !gll || (inr!=-1 && gll==1) ) return;
	/* start over, if the number is larger than the database's length */
	if( gll < inr ) inr = -1;

	/* If randomising, images are already randomised; hence we just
	   increment index no matter what. */
	if( inr == -1) { /* -1 means change */
		if( ++pbg_ctx->cur_img >= gll ) {
			if( bgmon.randomise ) randomise_image_list();
			pbg_ctx->cur_img=0;
		}
		/* make sure GKrellM will save the image number */
		gkrellm_config_modified();
	} else pbg_ctx->cur_img = inr;
	fname = g_strdup( ((struct idb_entry *)g_list_nth( pbg_ctx->idb, pbg_ctx->cur_img )->data)->filename );



	/*
	 * Make command string
	 */
	pbg_ctx->seconds_left = bgmon.wait_seconds;
#if defined(WIN32)
	if (bgmon.parse_cmd_output) {
#endif

	for (ch = bgmon.command; *ch; ) {
		if (*ch++=='%') {
			switch (*ch) {
			case '%': if (count==0) count = 1; break;
			case 's': ++count; break;
			default:  count = 2;
			}
			if (*ch) ++ch;
		}
	}

	/* If there was exactly one '%s' in command use it as format */
	tmp = count==1
		? g_strdup_printf( bgmon.command, g_shell_quote(fname) )
		: g_strdup_printf( "%s %s", bgmon.command, g_shell_quote(fname) );
	GKBG_debug("command: %s", tmp);

#if defined(WIN32)
	}
#endif


	/*
	 * Parse output
	 */
	if (bgmon.parse_cmd_output) {

		if (!g_spawn_command_line_sync(tmp, &ch, NULL, &count, NULL)) {
			count = 1;
		}
		free(tmp);

		/* Failed */
		if (count) {
			pbg_ctx->seconds_left = 10;
			return;
		}

		/* Parse */
		for (tmp = ch; *ch; ) {
			for (tag = ch; *ch && *ch!=':'; ++ch);
			if (!*ch) break;
			*ch++ = 0;
			for (value = ch; *ch && *ch!='\n'; ++ch);
			if (!*ch) break;
			*ch++ = 0;

			if (!strcmp(tag, "file")) {
				if (fname) free(fname);
				fname = g_strdup(value);
			} else if (!strcmp(tag, "tooltip")) {
				if (tiptext) free(tiptext);
				tiptext = g_strdup(value);
			} else if (strcmp(tag, "time")) {
				continue;
			}

			if (*value=='+' || *value=='-') {
				count = strtol(value, &value, 0);
				if (!*value) pbg_ctx->seconds_left += count;
			} else if (*value=='*' || *value=='x') {
				val = strtod(value+1, &value);
				if (!*value && val>0) pbg_ctx->seconds_left *= val;
			} else if (*value=='/') {
				val = strtod(value+1, &value);
				if (!*value && val>0) pbg_ctx->seconds_left /= val;
			} else if (*value=='<') {
				count = strtol(value+1, &value, 0);
				if (!*value && value>0 && pbg_ctx->seconds_left<count)
					pbg_ctx->seconds_left = count;
			} else if (*value=='>') {
				count = strtol(value+1, &value, 0);
				if (!*value && value>0 && pbg_ctx->seconds_left>count)
					pbg_ctx->seconds_left = count;
			} else {
				count = strtol(value, &value, 0);
				if (!*value && count>=0) pbg_ctx->seconds_left = count;
			}
		}
		free(tmp);


#if !defined(WIN32)
	/*
	 * Don't parse output
	 */
	} else {
		g_spawn_command_line_async( tmp, NULL );
		g_free( tmp );
#endif
	}


#if defined(WIN32)
	/*
	 * Set background under Win32
	 */
	SystemParametersInfo(SPI_SETDESKWALLPAPER, 0, fname, 0);
#endif


	/*
	 * Set tool tip text
	 */
	if( tiptext == NULL && fname ) {
		for (tiptext = ch = fname; *ch; ++ch) {
			if (*ch=='/' && *(ch+1)) tiptext = ch+1;
		}
		tiptext = g_locale_to_utf8( tiptext, -1, NULL, NULL, NULL);
	}
	gtk_tooltips_set_tip( pbg_ctx->tip, panel->drawing_area, tiptext, NULL );
	gtk_tooltips_enable( pbg_ctx->tip );
	g_free( tiptext );
	g_free( fname );
}




static void update_decals_text( gchar *text )
{
	gchar *s, buf[48];

	if( pbg_ctx->locked ) return;

	text[0] = '\0';
	for( s=bgmon.format_string; *s!='\0'; s++ ) {
		buf[0] = *s; buf[1]='\0';
		if( *s == '$' && *(s+1) !='\0' )
		switch( *(s+1) ) {
			case 's':
				g_snprintf( buf, 12, "%d", pbg_ctx->seconds_left );
				s++;
				break;
			case 'S':
				g_snprintf( buf, 12, "%d", bgmon.wait_seconds - pbg_ctx->seconds_left );
				s++;
				break;
			case 'm':
				g_snprintf( buf, 12, "%d", pbg_ctx->seconds_left / 60 );
				s++;
				break;
			case 'M':
				g_snprintf( buf, 12, "%d", (bgmon.wait_seconds-pbg_ctx->seconds_left) / 60 );
				s++;
				break;
			case 't':
				if (bgmon.wait_seconds > 3600) { /* If over an hour, display hh:mm */
					gint hours = pbg_ctx->seconds_left / 3600;
					g_snprintf( buf, 12, "%.2d:%.2d",
						hours, (pbg_ctx->seconds_left - hours*3600)/ 60 );
				} else
					g_snprintf( buf, 12, "%.2d:%.2d",
						pbg_ctx->seconds_left / 60, pbg_ctx->seconds_left % 60 );
				s++;
				break;
			case 'T':
				if (bgmon.wait_seconds > 3600) { /* If over an hour, display hh:mm */
					gint hours = (bgmon.wait_seconds-pbg_ctx->seconds_left) / 3600;
					g_snprintf( buf, 12, "%.2d:%.2d",
						hours,
						(bgmon.wait_seconds-hours*3600-pbg_ctx->seconds_left) / 60 );
				} else
					g_snprintf( buf, 12, "%.2d:%.2d",
						(bgmon.wait_seconds-pbg_ctx->seconds_left) / 60,
						(bgmon.wait_seconds-pbg_ctx->seconds_left) % 60 );
				s++;
				break;
		}
		strncat( text, buf,
			(strlen(text)+strlen(buf)) > TEXTSIZE ? TEXTSIZE - strlen(text) : strlen(buf) );
	}
	text = g_locale_to_utf8( text, -1, NULL, NULL, NULL );
}

static void update_krell(void)
{
	if ( ! bgmon.display_krell ) return;
	gkrellm_update_krell( panel, krell_time, (gulong) bgmon.wait_seconds - pbg_ctx->seconds_left );
}

static void update_plugin(void)
{
	int w = 0, c = 0;
	gchar text[TEXTSIZE] = "locked";

	if(pGK->second_tick && !pbg_ctx->locked &&  !(pbg_ctx->seconds_left--)) {
		update_image(-1);
	}

	if(!(pGK->timer_ticks % 2)) return;

	if( !pbg_ctx->locked ) update_decals_text( text );

	/* No need to do these calculations every time... FIXME */
	if ( bgmon.center_text ) {
		GkrellmMargin *m =gkrellm_get_style_margins(gkrellm_panel_style(style_id));
		w = gkrellm_gdk_string_width(gkrellm_panel_textstyle(style_id)->font, text);
		c = (gkrellm_chart_width() - w ) / 2 - m->left;
	}
	gkrellm_decal_text_set_offset(decal_wu, c, 2);
	if ( bgmon.display_text )
		gkrellm_draw_decal_text( panel, decal_wu, text, -1 );

	update_krell();
	gkrellm_draw_panel_layers( panel );
}

static gint panel_expose_event( GtkWidget *widget, GdkEventExpose *ev)
{
	if( widget == panel->drawing_area ) {
		gdk_draw_pixmap( widget->window,
			widget->style->fg_gc[GTK_WIDGET_STATE (widget)],
			panel->pixmap,
			ev->area.x, ev->area.y,
			ev->area.x, ev->area.y,
			ev->area.width, ev->area.height );
	}
	return FALSE;
}

static gint cb_panel_scroll( GtkWidget *widget, GdkEventScroll *ev )
{
	gint shift_modifier = (ev->state & GDK_SHIFT_MASK) ? !bgmon.simple_scroll_adj : bgmon.simple_scroll_adj;
	gint prev_locked = pbg_ctx->locked;

	if (ev->direction == GDK_SCROLL_UP) {
		if( !shift_modifier ) pbg_ctx->locked = 1;
		else pbg_ctx->seconds_left += bgmon.scroll_adj_time;
	} else if (ev->direction == GDK_SCROLL_DOWN) {
		if( !shift_modifier && pbg_ctx->locked) {
			pbg_ctx->locked = 0;
			if( bgmon.reset ) pbg_ctx->seconds_left = bgmon.wait_seconds;
		} else if( shift_modifier ) {
			pbg_ctx->seconds_left -= bgmon.scroll_adj_time;
			if( pbg_ctx->seconds_left < 0 ) pbg_ctx->seconds_left = 1;
		}
	}

	/* make sure GKrellM will save the lock state if it changed */
	if ( prev_locked != pbg_ctx->locked ) gkrellm_config_modified();

	return FALSE;
}

static gint cb_button_press( GtkWidget *widget, GdkEventButton *ev )
{
	gint shift_modifier = (ev->state & GDK_SHIFT_MASK ) ? 1 : 0;

	switch( ev->button ) {
		/* left */
		case 1:
			if( shift_modifier ) { /* shift+left -> lock/unlock the bg */
				if( pbg_ctx->locked ) {
					pbg_ctx->locked = 0;
					if( bgmon.reset ) pbg_ctx->seconds_left = bgmon.wait_seconds;
				} else pbg_ctx->locked = 1;
				/* make sure GKrellM will save the lock state */
				gkrellm_config_modified();
			} else {
				update_image(-1);
			}
			break;
		/* middle */
		case 2:
			update_image_list(1);
			break;
		/* right */
		case 3:
			if( shift_modifier ) {/* shift+right -> lock/unlock the bg */
				if( pbg_ctx->locked ) {
					pbg_ctx->locked = 0;
					if( bgmon.reset ) pbg_ctx->seconds_left = bgmon.wait_seconds;
				} else pbg_ctx->locked = 1;
				/* make sure GKrellM will save the lock state */
				gkrellm_config_modified();
			} else
				gkrellm_open_config_window( monitor );
			break;
	}

	return FALSE;
}

static void create_plugin( GtkWidget *vbox, gint first_create )
{
	GkrellmPiximage *krell_img;
	GkrellmStyle *style;
	GkrellmTextstyle *ts;
	gchar text[TEXTSIZE] = "bgchg" ;

	gkrellm_vbox = vbox;

	if (first_create) panel = gkrellm_panel_new0 ();
	else gkrellm_destroy_decal_list( panel );

	style = gkrellm_meter_style( style_id );
	krell_img = gkrellm_krell_meter_piximage( style_id );
	ts = gkrellm_panel_textstyle( style_id );
	panel->textstyle = ts;

	krell_time = gkrellm_create_krell( panel, krell_img, style );
	gkrellm_monotonic_krell_values(krell_time, FALSE);
	gkrellm_set_krell_full_scale( krell_time, bgmon.wait_seconds, 1 );
	if ( ! bgmon.display_krell )
		gkrellm_remove_krell( panel, krell_time );

	decal_wu = gkrellm_create_decal_text( panel, "Apif0", ts, style, -1, -1, -1 );

	gkrellm_panel_configure( panel, NULL, style );
	gkrellm_panel_create( vbox, monitor, panel );

	gkrellm_draw_decal_text( panel, decal_wu, text, -1 );

	if(first_create) {
		g_signal_connect( G_OBJECT (panel->drawing_area), "expose_event",
				G_CALLBACK(panel_expose_event), NULL );
		g_signal_connect( G_OBJECT (panel->drawing_area), "button_press_event",
				G_CALLBACK(cb_button_press), NULL );
		g_signal_connect(G_OBJECT(panel->drawing_area),"scroll_event",
				G_CALLBACK(cb_panel_scroll), NULL);

		pbg_ctx = g_malloc( sizeof(struct bg_ctx) );
		memset( pbg_ctx, 0x00, sizeof(struct bg_ctx) );
		if (bgmon.remember_image_number) pbg_ctx->cur_img = bgmon.image_nr_last_run;
		else pbg_ctx->cur_img = -1;
	} else pbg_ctx->cur_img = -1;

	pbg_ctx->tip = gtk_tooltips_new();
	gtk_tooltips_enable( pbg_ctx->tip );

	pbg_ctx->bgchg_rand = g_rand_new_with_seed( (guint32)time(NULL) );

	if (bgmon.remember_locked_state) pbg_ctx->locked = bgmon.locked_last_run;
	else pbg_ctx->locked = 0;
	pbg_ctx->seconds_left = bgmon.wait_seconds;

	update_image_list(1);
	if (bgmon.change_on_load) update_image( pbg_ctx->cur_img );
	update_krell();

	gkrellm_draw_panel_layers( panel );
}

static gchar *plugin_info_text[] = {
	"<b>GKrellMBgChg ",
	"is a GKrellM plugin which periodically changes\n",
	"your desktop background. It also allows you to monitor the time\n",
	"between the updates in various ways. It is possible, to force a\n",
	"change by clicking on the panel.\n\n",
	"<h>Mouse Actions:\n\n",
	"<b>- Left ", "changes the background image and resets the timer,\n",
	"\t+<Shift> toggles the background lock.\n",
	"<b>- Middle ", "reloads the images from the image database\n",
	"\t(see \"Image Database\" below).\n",
	"<b>- Right ", "opens the GKrellM Background Changer config window.\n",
	"\t+<Shift> toggles the background lock.\n\n",
	"<b>Mouse Wheel Actions:\n\n",
	"<b>- Up ", "\"locks\" the current background image (if you like it very much).\n",
	"\t+<Shift> prolongs the timer by the seconds given below.\n",
	"<b>- Down ", "\"unlocks\" the image and the counter continues.\n",
	"\t+<Shift> shortens the timer by the seconds given below.\n",
	"The option \"Mouse wheel adjusts timer\" below exchanges the\n",
	"<Shift>-behaviour of the mouse wheel.\n\n",
	"<h>Configuration:\n\n",
	"<b>- Format String\n",
	"\tThe text output format is controlled by this string (default: $s).\n",
	"<b>\t$s ", "are the seconds that are remaining to the next update\n",
	"<b>\t$S ", "are the seconds that passed since the last change\n",
	"<b>\t$m ", "are the minutes that are remaining to the next update\n",
	"<b>\t$M ", "are the minutes that passed since the last change\n",
	"<b>\t$t ", "is the time remaining to the next update, displayed as '-mm:ss'\n",
	"<b>\t$T ", "is the time that passed since the last change, displayed as 'mm:ss'.\n\n",
#if defined(WIN32)
	"<b>- Background Info Command\n",
	"\tProgram to return information such as timeout, filename and tooltip.\n"
	"\tIt is used only if Use Background Info Command is checked.\n"
	"\tSee ", "<b>README: Tips and Tricks", " for info about output format\n",
	"\tand some ideas.\n"
	"\tdefault: empty",
	"<b>- Use Background Info Command\n",
	"\tWhether to use Background Info Command. This option is experimental.\n",
	"\tdefault: ", "<b>off\n\n",
#else
	"<b>- Background Change Command\n",
	"\tProgram to use to set the desktop background image (including args).\n",
	"\tIf the command has exactly one '%s' it will be replaced by the\n",
	"\tproperly quoted background file name.\n",
	"\tCommon cases are:\n",
	"\tfor plain X11: ", "<b>xsetbg -fullscreen\n",
	"\tusing Eterm's Esetroot: ", "<b>Esetroot -f\n",
	"\tfor Gnome2 set it to:\n\t\t",
	"<b>gconftool-2 --type=string --set /desktop/gnome/background/picture_filename\n",
	"\tand for KDE 3.x:\n\t\t",
	"<b>dcop kdesktop KBackgroundIface setWallpaper %s 6\n",
	"\tdefault: ", "<b>Esetroot -f\n\n",
	"<b>- Parse Background Change Command output\n",
	"\tWhether to parse output of Background Change Command. This allows\n",
	"\tsetting such information as tooltip and timeout from the command.\n",
	"\tSee ", "<b>README: Tips and Tricks", " for info about output format\n",
	"\tand some ideas. This option is experimental.\n",
	"\tdefault: ", "<b>off\n\n",
#endif
	"<b>- Image Database\n",
	"\tFull path to a file containing all the images (full path/line) to be\n",
	"\tused by the plugin. (e.g. the output from: ", "<b>find / -name *.jpg | sort", ")\n",
	"\tEntries starting with a '#' will be ", "<i>ignored.\n",
	"\tdefault: ", "<b>~/images.idb\n\n",
#if !defined(WIN32)
	"<b>- Auto update image list\n",
	"\tSelect whether the image list should be automatically updated \n",
	"\twhen the image database file is modified.\n",
	"\tdefault: ", "<b>off\n\n",
	"<b>- Ignore not found images\n",
	"\tIgnore image files that could not be found.\n",
	"\tdefault: ", "<b>off\n\n",
#endif
	"<b>- Randomise images\n",
	"\tSelect whether the image list should be randomised or not.\n",
	"<b>\tNote: ", "If it is not set, it will ", "<i>always ",
	"start at the first image in the list.\n",
	"\tdefault: ", "<b>on\n\n",
	"<b>- Reset timer on \"lock\" release\n",
	"\tReset the timer to the initial value when the \"image lock\" is released.\n",
	"\tdefault: ", "<b>off\n\n",
	"<b>- Reset timer on config changes\n",
	"\tReset the timer on config changes, i.e. when you hit \"apply\" button.\n",
	"\tdefault: ", "<b>off\n\n",
	"<b>- Change wallpaper on load\n",
	"\tChanges the wallpaper when the plugin loads.\n",
	"\tdefault: ", "<b>off\n\n",
	"<b>- Change wallpaper on config changes\n",
	"\tChanges the wallpaper when the config changes.\n",
	"\tdefault: ", "<b>off\n\n",
	"<b>- Remember \"locked\" state from last run\n",
	"\tRemembers whether the current wallpaper was \"locked\"\n",
	"\tor not when GKrellM last shut down. Use with change-on-load\n",
	"\toption off to load a new wallpaper ", "<i>only", " on request.\n",
	"\tdefault: ", "<b>off\n\n",
	"<b>- Remember image number from last run\n",
	"\tRemembers the image number from the database that was\n",
	"\tshown when GKrellM last shut down. It starts the next time with\n",
	"\tthis image if the image list didn't change.\n",
	"\tdefault: ", "<b>off\n\n",
	"<b>- Center text\n",
	"\tCenters the displayed text.\n",
	"\tdefault: ", "<b>on\n\n",
	"<b>- Display text\n",
	"\tToggles the text on or off.\n",
	"\tdefault: ", "<b>on\n\n",
	"<b>- Display krell/slider\n",
	"\tToggles the krell on or off.\n",
	"\tdefault: ", "<b>on\n\n",
	"<b>- Mouse wheel adjusts timer\n",
	"\tWhen selected, scrolling the mouse wheel adjusts the time\n",
	"\trather than \"lock\" the image. Otherwise the adjustment\n",
	"\tworks in combination with the <Shift>-key.\n",
	"\tdefault: ", "<b>off\n\n",
	"<b>- Mouse wheel adjusts the timer by nnn seconds\n",
	"\tThis is the amount of seconds by which the timer is adjusted\n",
	"\twhen scrolling the mouse wheel while holding the <Shift>-key.\n",
	"\tSee also \"Mouse wheel adjusts timer.\"\n",
	"\tdefault: ", "<b>60\n"
};

static void create_bgchg_tab( GtkWidget *tab )
{
	GtkWidget *tabs, *vbox, *hbox;
	GtkWidget *label, *frame;
	GtkWidget *text;
	GtkAdjustment *adj, *mouse_wheel_adj;
	gchar *about_text = NULL;

	tabs = gtk_notebook_new();
	gtk_notebook_set_tab_pos(GTK_NOTEBOOK(tabs),GTK_POS_TOP);
	gtk_box_pack_start(GTK_BOX(tab),tabs,TRUE,TRUE,0);

	/* options */
	frame = gtk_frame_new(NULL);
	gtk_container_border_width(GTK_CONTAINER(frame),3);
	label = gtk_label_new(_("Options"));
	gtk_notebook_append_page(GTK_NOTEBOOK(tabs),frame,label);

	vbox = gtk_vbox_new(FALSE,0);
	gtk_container_add(GTK_CONTAINER(frame),vbox);

	hbox = gtk_hbox_new(FALSE, 0);
	label = gtk_label_new(_("Format String"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 4);
	entry_format_str = gtk_entry_new_with_max_length(127);
	gtk_entry_set_text(GTK_ENTRY(entry_format_str),bgmon.format_string);
	gtk_box_pack_start(GTK_BOX(hbox), entry_format_str, FALSE, FALSE, 4);
	gtk_container_add(GTK_CONTAINER(vbox),hbox);

	hbox = gtk_hbox_new(FALSE, 0);
#if defined(WIN32)
	label = gtk_label_new(_("Background Info Command"));
#else
	label = gtk_label_new(_("Background Change Command"));
#endif
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 4);
	entry_command = gtk_entry_new_with_max_length(255);
	gtk_entry_set_text(GTK_ENTRY(entry_command),bgmon.command);
	gtk_box_pack_start(GTK_BOX(hbox), entry_command, TRUE, TRUE, 4);
	gtk_container_add(GTK_CONTAINER(vbox),hbox);

#if defined(WIN32)
	parse_cmd_entry = gtk_check_button_new_with_label( _("Use Background Info Command (experimental)"));
#else
	parse_cmd_entry = gtk_check_button_new_with_label( _("Parse Background Change Command output (experimental)"));
#endif
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( parse_cmd_entry ), bgmon.parse_cmd_output );
	gtk_container_add( GTK_CONTAINER(vbox), parse_cmd_entry );


	hbox = gtk_hbox_new(FALSE, 0);
	label = gtk_label_new(_("Image Database"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 4);
	entry_idb = gtk_entry_new_with_max_length(255);
	gtk_entry_set_text(GTK_ENTRY(entry_idb),bgmon.idb);
	gtk_box_pack_start(GTK_BOX(hbox), entry_idb, TRUE, TRUE, 4);
	gtk_container_add(GTK_CONTAINER(vbox),hbox);

	hbox = gtk_hbox_new(FALSE, 0);
	adj = (GtkAdjustment *) gtk_adjustment_new(bgmon.wait_seconds,
			1.0, 99999.0, 1.0, 5.0, 0.0);
	wait_seconds_spin_button = gtk_spin_button_new(adj, 0.5, 0);
	gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(wait_seconds_spin_button), TRUE);
	gtk_box_pack_start(GTK_BOX(hbox), wait_seconds_spin_button, FALSE, FALSE, 4);
	label = gtk_label_new(_("Seconds between image changes"));
	gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_LEFT);
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 4);
	gtk_container_add(GTK_CONTAINER(vbox),hbox);

#if !defined(WIN32)
	auto_update_entry = gtk_check_button_new_with_label( _("Auto update image list"));
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( auto_update_entry ), bgmon.auto_update );
	gtk_container_add( GTK_CONTAINER(vbox), auto_update_entry );
#endif

	ignore_entry = gtk_check_button_new_with_label( _("Ignore not found images"));
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( ignore_entry ), bgmon.ignore );
	gtk_container_add( GTK_CONTAINER(vbox), ignore_entry );

	randomise_entry = gtk_check_button_new_with_label( _("Randomise images"));
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( randomise_entry ), bgmon.randomise );
	gtk_container_add( GTK_CONTAINER(vbox), randomise_entry );

	reset_entry = gtk_check_button_new_with_label( _("Reset timer on \"lock\" release"));
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( reset_entry ), bgmon.reset );
	gtk_container_add( GTK_CONTAINER(vbox), reset_entry );

	reset_entry2 = gtk_check_button_new_with_label( _("Reset timer on config changes"));
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( reset_entry2 ), bgmon.reset_config );
	gtk_container_add( GTK_CONTAINER(vbox), reset_entry2 );

	change_on_load = gtk_check_button_new_with_label(_("Change wallpaper on load"));
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( change_on_load ), bgmon.change_on_load );
	gtk_container_add( GTK_CONTAINER(vbox), change_on_load );

	change_on_apply = gtk_check_button_new_with_label(_("Change wallpaper on config changes"));
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( change_on_apply ), bgmon.change_on_apply );
	gtk_container_add( GTK_CONTAINER(vbox), change_on_apply );

	remember_locked_state = gtk_check_button_new_with_label(_("Remember \"locked\" state from last run"));
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( remember_locked_state ), bgmon.remember_locked_state );
	gtk_container_add( GTK_CONTAINER(vbox), remember_locked_state );

	remember_image_number = gtk_check_button_new_with_label(_("Remember image number from last run"));
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( remember_image_number ), bgmon.remember_image_number );
	gtk_container_add( GTK_CONTAINER(vbox), remember_image_number );

	center_text = gtk_check_button_new_with_label(_("Center text"));
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( center_text ), bgmon.center_text );
	gtk_container_add( GTK_CONTAINER(vbox), center_text );

	display_text = gtk_check_button_new_with_label(_("Display text"));
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( display_text ), bgmon.display_text );
	gtk_container_add( GTK_CONTAINER(vbox), display_text );

	display_krell = gtk_check_button_new_with_label(_("Display krell/slider"));
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( display_krell ), bgmon.display_krell );
	gtk_container_add( GTK_CONTAINER(vbox), display_krell );

	simple_scroll_adj = gtk_check_button_new_with_label(_("Mouse wheel adjusts timer"));
	gtk_toggle_button_set_active( GTK_TOGGLE_BUTTON( simple_scroll_adj ), bgmon.simple_scroll_adj );
	gtk_container_add( GTK_CONTAINER(vbox), simple_scroll_adj );

	hbox = gtk_hbox_new (FALSE, 5);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, TRUE, TRUE, 0);

	label = gtk_label_new (_("Mouse wheel adjusts the timer by"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);

	mouse_wheel_adj = (GtkAdjustment *) gtk_adjustment_new (bgmon.scroll_adj_time, 1, 1000, 1, 10, 0);
	scroll_adj_spin_button = gtk_spin_button_new (GTK_ADJUSTMENT (mouse_wheel_adj), 1, 0);
	gtk_widget_show (scroll_adj_spin_button);
	gtk_box_pack_start (GTK_BOX (hbox), scroll_adj_spin_button, FALSE, TRUE, 0);

	label = gtk_label_new (_("second(s)"));
	gtk_widget_show (label);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);

	/* info */
	vbox = gkrellm_gtk_framed_notebook_page(tabs,_("Info"));
	text = gkrellm_gtk_scrolled_text_view(vbox, NULL,
			GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gkrellm_gtk_text_view_append_strings(text, _(plugin_info_text),
			sizeof(plugin_info_text) / sizeof(gchar *));

	/* about */
	about_text = g_strdup_printf(
		"GKrellMBgChg %s\n" \
		"GKrellM Background Changer Plugin\n\n" \
		"Copyright © 2002-2010 Stefan Bender\n" \
		"stefan@bender-suhl.de\n" \
		"http://www.bender-suhl.de/stefan/english/comp/gkrellmbgchg.html\n\n" \
		"Released under the GNU General Public Licence",
		GKRELLMBGCHG_VERSION);

	text = gtk_label_new(about_text);
	label = gtk_label_new(_("About"));
	gtk_notebook_append_page(GTK_NOTEBOOK(tabs),text,label);
	g_free(about_text);
}

static void apply_config(void)
{
	const gchar *s;

	/* update config vars */
	bgmon.wait_seconds = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(wait_seconds_spin_button));
	s = gtk_entry_get_text(GTK_ENTRY(entry_format_str));
	strcpy(bgmon.format_string,s);
	s = gtk_entry_get_text(GTK_ENTRY(entry_idb));
	strcpy(bgmon.idb,s);
#if !defined(WIN32)
	bgmon.auto_update = GTK_TOGGLE_BUTTON( auto_update_entry )->active;
#endif
	bgmon.ignore = GTK_TOGGLE_BUTTON( ignore_entry )->active;
	s = gtk_entry_get_text(GTK_ENTRY(entry_command));
	strcpy(bgmon.command,s);
	bgmon.parse_cmd_output = GTK_TOGGLE_BUTTON( parse_cmd_entry )->active;
	bgmon.randomise = GTK_TOGGLE_BUTTON( randomise_entry )->active;
	bgmon.reset = GTK_TOGGLE_BUTTON( reset_entry )->active;
	bgmon.reset_config = GTK_TOGGLE_BUTTON( reset_entry2 )->active;
	bgmon.change_on_load = GTK_TOGGLE_BUTTON( change_on_load )->active;
	bgmon.change_on_apply = GTK_TOGGLE_BUTTON( change_on_apply )->active;
	bgmon.remember_locked_state = GTK_TOGGLE_BUTTON( remember_locked_state )->active;
	bgmon.remember_image_number = GTK_TOGGLE_BUTTON( remember_image_number )->active;
	bgmon.simple_scroll_adj = GTK_TOGGLE_BUTTON( simple_scroll_adj )->active;
	bgmon.display_text = GTK_TOGGLE_BUTTON( display_text )->active;
	bgmon.center_text = GTK_TOGGLE_BUTTON( center_text )->active;
	bgmon.display_krell = GTK_TOGGLE_BUTTON( display_krell )->active;

	/* create new panel, not the first time */
	if( bgmon.reset_config ) pbg_ctx->seconds_left = bgmon.wait_seconds;
	update_image_list(1);
	if( bgmon.change_on_apply ) update_image(-1);

	if ( bgmon.display_text )
		gkrellm_make_decal_visible( panel, decal_wu );
	else
		gkrellm_make_decal_invisible( panel, decal_wu );
	if ( bgmon.display_krell )
		gkrellm_insert_krell( panel, krell_time, 1 );
	else
		gkrellm_remove_krell( panel, krell_time );
}

static void save_config( FILE *f)
{
	fprintf( f, "%s wait_seconds %d\n", CONFIG_KEYWORD, bgmon.wait_seconds );
#if !defined(WIN32)
	fprintf( f, "%s auto_update %d\n", CONFIG_KEYWORD, bgmon.auto_update );
#endif
	fprintf( f, "%s ignore %d\n", CONFIG_KEYWORD, bgmon.ignore );
	fprintf( f, "%s command %s\n", CONFIG_KEYWORD, bgmon.command );
	fprintf( f, "%s parse_cmd_output %d\n", CONFIG_KEYWORD, bgmon.parse_cmd_output );
	fprintf( f, "%s randomise %d\n", CONFIG_KEYWORD, bgmon.randomise );
	fprintf( f, "%s reset %d\n", CONFIG_KEYWORD, bgmon.reset );
	fprintf( f, "%s reset_config %d\n", CONFIG_KEYWORD, bgmon.reset_config );
	fprintf( f, "%s format_string %s\n", CONFIG_KEYWORD, bgmon.format_string );
	fprintf( f, "%s idb %s\n", CONFIG_KEYWORD, bgmon.idb );
	fprintf( f, "%s change_on_load %d\n", CONFIG_KEYWORD, bgmon.change_on_load );
	fprintf( f, "%s change_on_apply %d\n", CONFIG_KEYWORD, bgmon.change_on_apply );
	fprintf( f, "%s remember_locked_state %d\n", CONFIG_KEYWORD, bgmon.remember_locked_state );
	fprintf( f, "%s locked_last_run %d\n", CONFIG_KEYWORD, pbg_ctx->locked );
	fprintf( f, "%s remember_image_number %d\n", CONFIG_KEYWORD, bgmon.remember_image_number );
	if (!pbg_ctx->idb || pbg_ctx->cur_img < 0) {
		fprintf( f, "%s image_nr_last_run %d\n", CONFIG_KEYWORD, 0 );
	} else if (!pbg_ctx->idb_orig) {
		fprintf( f, "%s image_nr_last_run %d\n", CONFIG_KEYWORD,
		         pbg_ctx->cur_img );
	} else {
		GList *list = g_list_nth(pbg_ctx->idb, pbg_ctx->cur_img);
		fprintf( f, "%s image_nr_last_run %d\n", CONFIG_KEYWORD,
		         list ? g_list_index( pbg_ctx->idb_orig, list->data) : 0);
	}
	fprintf( f, "%s simple_scroll_adj %d\n", CONFIG_KEYWORD, bgmon.simple_scroll_adj );
	fprintf( f, "%s scroll_adj_time %d\n", CONFIG_KEYWORD, bgmon.scroll_adj_time );
	fprintf( f, "%s center_text %d\n", CONFIG_KEYWORD, bgmon.center_text );
	fprintf( f, "%s display_text %d\n", CONFIG_KEYWORD, bgmon.display_text );
	fprintf( f, "%s display_krell %d\n", CONFIG_KEYWORD, bgmon.display_krell );
}

static void load_config( gchar *arg )
{
	gchar *command, *p;

	for( p = arg; *p && isspace(*p); p++ );
	for( ; *p && !isspace(*p); p++ );

	command = g_malloc( (p-arg+1)*sizeof(char) );
	memset( command, 0x00, p-arg+1 );
	memcpy( command, arg, p-arg );

	for( ; *p && isspace(*p); p++ );

	if( !strcmp(command, "wait_seconds") ) bgmon.wait_seconds = atoi( p );
#if !defined(WIN32)
	else if( !strcmp(command, "auto_update") ) bgmon.auto_update = atoi( p );
#endif
	else if( !strcmp(command, "ignore") ) bgmon.ignore = atoi( p );
	else if( !strcmp(command, "command") ) strcpy( bgmon.command, p );
	else if( !strcmp(command, "parse_cmd_output") ) bgmon.parse_cmd_output = atoi( p );
	else if( !strcmp(command, "randomise") ) bgmon.randomise = atoi( p );
	else if( !strcmp(command, "reset") ) bgmon.reset = atoi( p );
	else if( !strcmp(command, "reset_config") ) bgmon.reset_config = atoi( p );
	else if( !strcmp(command, "format_string") ) strcpy( bgmon.format_string, p );
	else if( !strcmp(command, "idb") ) strcpy( bgmon.idb, p );
	else if( !strcmp(command, "change_on_load") ) bgmon.change_on_load = atoi( p );
	else if( !strcmp(command, "change_on_apply") ) bgmon.change_on_apply = atoi( p );
	else if( !strcmp(command, "remember_locked_state") ) bgmon.remember_locked_state = atoi( p );
	else if( !strcmp(command, "locked_last_run") ) bgmon.locked_last_run = atoi( p );
	else if( !strcmp(command, "remember_image_number") ) bgmon.remember_image_number = atoi( p );
	else if( !strcmp(command, "image_nr_last_run") ) bgmon.image_nr_last_run = atoi( p );
	else if( !strcmp(command, "simple_scroll_adj") ) bgmon.simple_scroll_adj = atoi( p );
	else if( !strcmp(command, "scroll_adj_time") ) bgmon.scroll_adj_time = atoi( p );
	else if( !strcmp(command, "center_text") ) bgmon.center_text = atoi( p );
	else if( !strcmp(command, "display_text") ) bgmon.display_text = atoi( p );
	else if( !strcmp(command, "display_krell") ) bgmon.display_krell = atoi( p );

	g_free( command );
}

void bgchg_atexit()
{
	g_free( pbg_ctx );
}

static GkrellmMonitor plugin_mon = {
	.name = CONFIG_NAME,
	.id = 0,
	.create_monitor = create_plugin,
	.update_monitor = update_plugin,
	.create_config = create_bgchg_tab,
	.apply_config = apply_config,
	.save_user_config = save_config,
	.load_user_config = load_config,
	.config_keyword = CONFIG_KEYWORD,
	.undef2 = NULL,
	.undef1 = NULL,
	.privat = NULL,
	.insert_before_id = PLACEMENT,
	.handle = NULL,
	.path = NULL
};

#if defined(WIN32)
__declspec(dllexport) GkrellmMonitor *
gkrellm_init_plugin(win32_plugin_callbacks* calls)
#else
GkrellmMonitor *gkrellm_init_plugin()
#endif
{
#if defined(WIN32)
	callbacks = calls;
#endif

	g_atexit( bgchg_atexit );

	pGK = gkrellm_ticks();
	style_id = gkrellm_add_meter_style( &plugin_mon, STYLE_NAME );
	monitor = &plugin_mon;
	return &plugin_mon;
}
