#!/bin/sh
# kdewallpaper.sh for gkrellmbgchg
# standalone usage: kdewallpaper.sh <full path of image file>
#
# for kde < 3.3
dcop kdesktop KBackgroundIface setWallpaper $1 6
#
# for kde >~3.3 the desktop can be specified:
#dcop kdesktop KBackgroundIface setWallpaper 4 $1 6
#                  the number of the desktop ^
#
# the modes (last number above) are:
# 0 = NoWallpaper
# 1 = Centred
# 2 = Tiled
# 3 = CenterTiled
# 4 = CentredMaxpect
# 5 = TiledMaxpect
# 6 = Scaled
# 7 = CentredAutoFit
# 8 = ScaleAndCrop
# 9 = lastWallpaperMode
