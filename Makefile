# Makefile for the GKrellM Background Changer plugin

# gets version information from git (adapted from the tig Makefile)
ifneq (,$(wildcard .git))
GITDESC	= $(subst gkrellmbgchg2-,,$(shell git describe))
WTDIRTY	= $(if $(shell git diff-index HEAD 2>/dev/null),-dirty)
VERSION	= $(GITDESC)$(WTDIRTY)
override CPPFLAGS += '-DGKRELLMBGCHG_VERSION="$(VERSION)"'
endif

GTK_CONFIG ?=pkg-config

GTK_INCLUDE = `$(GTK_CONFIG) gtk+-2.0 --cflags`
GTK_LIB = `$(GTK_CONFIG) gtk+-2.0 --libs`

GKRELLM_INCLUDE= -I/usr/local/include

INSTALL=install -c -m 755

CC ?= gcc
CFLAGS += -Wall -fPIC $(GTK_INCLUDE) $(GKRELLM_INCLUDE)
LIBS = $(GTK_LIB) 
LDFLAGS += -shared

OBJS = gkrellmbgchg.o

all: gkrellmbgchg.so

gkrellmbgchg.so: $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o gkrellmbgchg.so $(LDFLAGS) $(LIBS)

clean:
	rm -f *.o core *.so* *.bak *~
install: gkrellmbgchg.so
	$(INSTALL) -d $(HOME)/.gkrellm2/plugins
	$(INSTALL) gkrellmbgchg.so $(HOME)/.gkrellm2/plugins

gkrellmbgchg.c.o: gkrellmbgchg.c

