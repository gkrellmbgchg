#ifndef __GKRELLMBGCHG_H_
#define __GKRELLMBGCHG_H_

#if !defined(WIN32)
#include <gkrellm2/gkrellm.h>
#else
#include <src/gkrellm.h>
#include <src/win32-plugin.h>
#endif

#define CONFIG_NAME "Background Changer"
#define CONFIG_KEYWORD "bgchg"
#define STYLE_NAME "bgchg"
#ifndef GKRELLMBGCHG_VERSION
#define GKRELLMBGCHG_VERSION "0.1.11"
#endif

#if defined(MAXPATHLEN)
  #define BUFFSIZE MAXPATHLEN
#else
  #if defined(PATH_MAX)
	#define MAXPATHLEN PATH_MAX
	#define BUFFSIZE PATH_MAX
  #endif
  #define BUFFSIZE 4096
#endif

#define TEXTSIZE 128

#ifdef GRAVITY
#define PLACEMENT (MON_UPTIME | GRAVITY(12))
#else
#define PLACEMENT (MON_UPTIME)
#endif

#endif /* __GKRELLMBGCHG_H_ */
