##
## Usage: bgchg_info <tag> <value>
##            Simply prints   <tag>:<value>   in a line.
##        bgchg_info <tooltip>
##            Equivalent to   bgchg_info tooltip <tooltip>
##        bgchg_info <tag> <format> <arg1> ...
##            Equivalent to   bgchg_info <tag> "$(printf <format> <arg1> ...)"
##
## The function does not check if the given arguments are valid.  In
## particular it doesn't check if <tag> doesn't contain a colon or
## <value> a new line.
##
bgchg_info () {
	if [ $# -eq 1 ]; then
		printf 'tooltip:%s\n' "$1"
	elif [ $# -eq 2 ]; then
		printf '%s:%s\n' "$1" "$2"
	elif [ $# -gt 2 ]; then
		printf %s:  "$1"
		shift
		printf "$@"
		echo
	fi
}
